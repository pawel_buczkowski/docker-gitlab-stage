FROM bash

RUN set -x \
    && apk add \
		curl \
		openssh \
		sshpass \
		git
